"""Interface for the Sensor Correction fader documented in T1800414.

Should be similar in function to the ligoblend.py but a bit simpler.

A few notes about the fader.c code:
* Setting the ST1_SENSCOR_{X,Y,Z}_NEXT_CHAN epics to a number listed
below, will automatically start the fader c code. 
* If it is set to 0 then te SC output will be ramped to 0.
* Once the c code starts, the channel cannot be changed, and the 
fade cannot be stopped (just like the blends when the FE starts).
* If the Cur chan and Next chan values are different, then a fade
is in progress. The time left chan will also count down.

Normal filters      - 1-4
EQ filters          - 5-7
Uncorrected filters - 8-9

"""
import time

DEFAULT_RAMP_TIME = 30
FADE_NEXT_CHAN = 'NEXT_CHAN'
FADE_CUR_CHAN = 'FADE_CUR_CHAN_MON'
FADE_TIME_LEFT_CHAN = 'FADE_TIME_LEFT_MON'
FADE_RAMP_TIME_CHAN = 'TRAMP'
FADE_OUTPUT_CHAN = 'FADE_MON'

def filter_type(fm):
    if fm in range(1,5):
        return 'Normal'
    elif fm in range(5,8):
        return 'EQ'
    elif fm in range(8,11):
        return 'Uncorrected'
    else:
        return 'Output Off'

########################################

class FaderError(Exception):
    pass

########################################
# General use interface, works with with or without ezca prefixes
########################################

class Fader(object):
    """Interface for the Sensor Correction Fader documented in T1800414.
    

    """
    def __init__(self, senscor_bank):
        """To allow for this to be used in Guardian and in scripts,
        the arg needs to be able to handle banks with and without
        ezca prefixes.

        senscor_bank - full name of the senscor bank after any previously
                       specified prefixes.
                       (Examples:
                           with prefix='ISI-ETMX_ST1_', senscor_bank='SENSCOR_X'
                           w/o prefix, senscor_bank='ISI_EMTX_ST1_SENSCOR_X'
        """
        self.bank = senscor_bank
        self.next_chan = '_'.join([self.bank, FADE_NEXT_CHAN])
        self.cur_chan = '_'.join([self.bank, FADE_CUR_CHAN])
        self.time_left_chan = '_'.join([self.bank, FADE_TIME_LEFT_CHAN])
        self.ramp_time_chan = '_'.join([self.bank, FADE_RAMP_TIME_CHAN])
        self.output_chan = '_'.join([self.bank, FADE_OUTPUT_CHAN])
        # Will this always be true???
        self.dof = self.bank[-1]

        
    def start_fade(self, fm_num, ramp=DEFAULT_RAMP_TIME, wait=False):
        """Start the fade by entering a value 0-9 into the NEXT_CHAN
        epics variable.
        
        fm_num - An integer from 0-9
        ramp   - Ramp time
        wait   - If True, will wait for the fade to complete before
                 moving on.
        """
        # Check fm_num is valid
        if fm_num not in range(11):
            raise FaderError('Requested filter number \'{}\' does not exist'.format(fm_num))
        # Check not already fading
        if self.is_fading:
            log('Fade already in progress. Waiting for it to complete, then trying again.')
            while self.is_fading:
                continue
            # Needs to wait slightly after the old fade is complete
            time.sleep(1)
        # Same filter request
        if fm_num == ezca[self.cur_chan]:
            return

        log('Starting fade for {dof} dof to Filt: {fm}, Type: {tp}'.format(dof=self.dof,
                                                                           fm=fm_num,
                                                                           tp=filter_type(fm_num)))
        if ramp != ezca[self.ramp_time_chan]:
            ezca[self.ramp_time_chan] = ramp
        # By changing the epics value, the FE will take control and
        # run the C code. It is out of our hands now
        ezca[self.next_chan] = fm_num

        # FIXME: This is due to a bug in the C code. Every so often,
        # the write to the channel will be reverted ~0.1-0.2s later.
        # We have to check to make sure that it is actually fading,
        # or else try again. We will only try once, and a check in
        # the run method will also be there.
        # This bug will be fixed soon, I imagine.
        time.sleep(1)
        if ezca[self.next_chan] != fm_num:
            log('FE did not accept first attempt, trying again.')
            ezca[self.next_chan] = fm_num

            
        if wait:
            log('Waiting for the fader to complete...')
            time.sleep(ezca[self.ramp_time_chan])
            # Wait a bit more if it is not done
            while self.is_fading:
                time.sleep(0.1)
            log('Fading complete')

            
    def zero_output(self):
        """Convenience method for readability"""
        self.start_fade(0)


    @property
    def is_fading(self):
        if ezca[self.cur_chan] != ezca[self.next_chan] and\
           ezca[self.time_left_chan] > 0:
            return True
        else:
            return False


    @property
    def has_output(self):
        output = ezca[self.output_chan]
        if abs(output) > 0:
            return True
        else:
            return False


    def get_cur_filter(self):
        """Return a tuple of the number and type of filter currently in use."""
        cur_fm = ezca[self.cur_chan]
        return (int(cur_fm), filter_type(cur_fm))




class FaderManager(object):
    """Manage grouped Fader's"""
    def __init__(self, senscor_bank_list):
        self.banks = senscor_bank_list
        # Will this always be true????
        self.dofs = [bank[-1] for bank in self.banks]
        self.senscor_banks = {}
        for bank in self.banks:
            self.senscor_banks[bank] = Fader(bank)

    def all_off(self):
        """Zero all of the outputs in dofs"""
        for bank_name, bank in self.senscor_banks.items():
            bank.zero_output()

    def engage(self, fm_num, wait=False):
        for bank_name, bank in self.senscor_banks.items():
            bank.start_fade(fm_num, wait=wait)
            
    @property
    def any_fading(self):
        for bank_name, bank in self.senscor_banks.items():
            if bank.is_fading:
                return True
        return False

    @property
    def any_output(self):
        outputs = []
        for bank_name, bank in self.senscor_banks.items():
            if bank.has_output:
                outputs.append(bank)
        if outputs:
            return True
        else:
            return False
